---
title: The Plan
subtitle: Everyone needs a BHAG.
comments: false
---

# The Plan

The end goal is a collection of immortal, interstellar travelling humans. 

Aim big, ya know?

Also, travelling through the stars by oneself is lonely, so open-sourcing the technology is a must.

Of course, this is a *_slightly_* ambitious goal, and a direct route isn't going to be viable.

Thus, it's broken into 3 main stages.

## Earth
The Earth is where it all begins. Here we start with the basics, creation of technology to build and construct self-sustaining and self-constructing seed factories.
This is broken out into two over-arching parts, software and hardware.

### Software
Building hardware is hard. Building the same hardware twice is even harder. Thus, harp.

Harp stands for *Har*dware *P*acager. It a series of schemas to describe all of the parts and pieces of hardware to completely construct another. It facilitates a tree of hardware dependancies. Screws go into plates, which go into ships. It aims to be generic, in that you can use the same software to plan a car, a wind turbine, or an omelette. Using this software we can start designing _things._ Things we can build under the flags of open-source, sustainable and green technology. Later goals are impossible without being environmentally friendly. There is no room for waste.

### Hardware
With planning, hardware can be easier to construct. Harp can provide that. The next goal is to design and construct the tools to design and construct more things.
The initial push is to identify the key machines and materials to that can serve as the seed to build anything. After these key tools are identified, small, community sized factories can be constructed, further fueling our expansion.

### The Seed Factory
The Seed Factory is our, and humanities, MVP. A miniumum set of tools and processes to branch out and build anything, with minimal tweaking to account for local resource variability. With some tweaks, we can move on to the next phase of the project.

## The Moon
This is where we get our 'space legs'. Ideally, by the time we get to this stage (2025), there should be many options for commercial launches, enabling relativly inexpensive access to the moon. Here, we will start design and manufacture of space capable power generation and resource utilization equipment. The plan is to extend our manufacturing base on Earth to here, overcoming the natural challenges of vacuum and space in general. Construction of our own space craft and machines to mine surfaces of empty worlds will lead us to reaching Mercury.

## The Sun